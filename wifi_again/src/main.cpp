#include <WiFiUdp.h>
#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
//////ArduinoJson_ID64
#include "ArduinoJson.hpp"
#include <ArduinoJson.h>
#include <NtpClient.h>
//#include <TimeLib.h>


//////
/////////////Variables utilizadas para obtener el tiempo///////////

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "0.south-america.pool.ntp.org", 3600, 60000);
////////////// variables utilizadas para realizar el time stamp//////////////
String formattedDate;
String dayStamp;
String timeStamp;

//Variables utilizadas para la conexión WiFi

const char* ssid     = "Fibertel WiFi396 2.4GHz";
const char* password = "0049414836";
char* status_wifi ="";
char* ip_address="";
char* mac_adress="";

byte mac[6];

IPAddress Ip;
long rssi = 0;
/////////////////////////////////////////////////

////////////////////Variables destinadas a la identificación del dispositivo y aplicación/////////////////
char mac_char[32] = "";  //variable utilizada para almacenar la dirección MAC como string. 
char token_dev[21]= "PFVLE7rC1s5MRTdV58bf";
char id_app[21] = "nkQSbTEOFJewnPpCH0q6";
char token_app[21]= "jgzzEPPQ0pTePTpt50x8";
//movimiento no será tenido en cuenta por ahora
int movim_status=0;
int movim_pin=10;
////////////////////////////////////////////////
int status_llamada=0;   //status general del pulsado
int timer_activo=0;     //status del timer: =0->ya finalizó el conteo de xx segundos, sino finalizó es =1.
int bot_llamar=0;
int bot_cancelar=0;
int enviar=0;           //=0->no envia; =1->envia dato
//pines utilizador para cada entrada
int bot_llamar_pin=34;
int bot_cancelar_pin=35;
//Datos de la batería.
float bateria=0.0;
//variable utilizada para enviar si hay errores en los procesamientos previos
int error=0;        // 0 -> sin errores anteriores.
                    // 1 -> error de conexión wifi
                    // 2 -> error al enviar dato; quizás deba mostrar el numero de error que se produjo
                    // 3 -> 
//Led Indicador
int led_pin=22;
int led_valor = 0;

StaticJsonDocument<300> doc;
DynamicJsonDocument doc1(1024);
char test[200];



//////////////Variables auxiliares//////////


int temporal = 0;


//JSON data:
/*
{ 
"id_dev": "Dirección_MAC del device",
"token_dev": "token del device",
"id_app": "ID de la Aplicación",
"token_app": "Token de la App",
"Data":[
    time_stamp:
    Boton:
    Bateria:
    Error:
        ]
}
*/
//const char* json = "{\"id\":\"6C:88:E6:3A:7D:80\",\"time\":1351824120,\"data\":[48.75608,2.302038]}";
//const size_t capacity = JSON_ARRAY_SIZE(2) + JSON_OBJECT_SIZE(3) + 40;
void conexion()
{
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    timeClient.begin();
    timeClient.setTimeOffset(-10800);
    Serial.println("");
    Ip=WiFi.localIP();
    WiFi.macAddress(mac);   //obtenccion de dirección MAC

    /////// Aviso de Conexión /////
    Serial.println("WiFi connected");
    
    /////// Datos relevantes a la conexión //////
    Serial.println("IP address: ");
    Serial.println(Ip);
    //Serial.println(mac);
    ////// MAC  //////
    Serial.print("MAC: ");
    Serial.print(mac[0],HEX);
    Serial.print(":");
    Serial.print(mac[1],HEX);
    Serial.print(":");
    Serial.print(mac[2],HEX);
    Serial.print(":");
    Serial.print(mac[3],HEX);
    Serial.print(":");
    Serial.print(mac[4],HEX);
    Serial.print(":");
    Serial.println(mac[5],HEX);
    ////// RSSI  //////
    Serial.print("RSSI: ");
    rssi=WiFi.RSSI();
    Serial.print(rssi);
    Serial.println("");
    
}

/////////////// array_to_string da error si se procesa después de enviar_post ///////////////
void array_to_string(byte array[], unsigned int len, char buffer[])
{
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';
}
void enviar_post(){
HTTPClient http;   
//http.begin("http://cvsingenieria.mybluemix.net/boton?");  //Specify destination for HTTP request
http.begin("http://192.168.0.60:1880/boton?");  //Specify destination for HTTP request
http.addHeader("Content-Type", "text/plain");             //Specify content-type header
Serial.println("Archivo JSON: ");

Serial.println("------------Serializando------------");
Serial.println();
serializeJson(doc, test);
serializeJson(doc, Serial);
Serial.println();
Serial.println("-----------------------");
Serial.println("------------Preety------------");
Serial.println();
serializeJsonPretty(doc, Serial);
Serial.println();
Serial.println("-----------------------");
Serial.println();

  // Generate the prettified JSON and send it to the Serial port.
  //
    
   delay(1000);
   //Serial.println(json);
   int httpResponseCode = http.POST(test);   //Send the actual POST request
   
   if(httpResponseCode>0){
 
    String response = http.getString();                       //Get the response to the request
 
    Serial.println(httpResponseCode);   //Print return code
    Serial.println(response);           //Print request answer
 
   }else{
 
    Serial.print("Error on sending POST: ");
    Serial.println(httpResponseCode);
 
   }
 
   http.end();  //Free resources

}
void crearJson()
{
 
doc["id_dev"] = mac_char;
doc["token_dev"] = token_dev;
doc["id_app"] = id_app;
doc["token_app"] = token_app;

}

void grabar_datos()
{
    /////////////////así se graba en un objeto////////////
    doc["token_app"]=random(100);
    /////////////////////////////////////////////////////
    /////////////////así se graba en un array////////////
    unsigned long epcohTime =  timeClient.getEpochTime();   //time_stamp!
    doc["data"]["time_stamp"]= epcohTime;
    /////////////////////////////////////////////////////
    doc["data"]["Boton1"]= bot_llamar;
    doc["data"]["error"]= 1;
    doc["data"]["valor"]= 1;

}

void leer_boton()
{
    bot_llamar=digitalRead(bot_llamar_pin);
    digitalWrite(led_pin, bot_llamar);
    Serial.printf("Boton: %d",bot_llamar);
    if (status_llamada==0)
        {
            if (bot_llamar==0)
                {
                status_llamada=1;
                enviar=1;
                }
        }
    
    
    
    Serial.println();

}


void setup()
{
    Serial.begin(115200);
    delay(10);
    pinMode(led_pin, OUTPUT);
    pinMode(bot_llamar_pin, INPUT);
    /////////////Se realiza la primer conexión al router WiFi/////////////////
    conexion();
    /////////////Conversion de MAC/////////////////
    array_to_string(mac, 6, mac_char);
    Serial.println(mac_char);
    //////////////////////////////////////////////
    crearJson();

}

void loop() {
    
    while(!timeClient.update()) {
        timeClient.forceUpdate();
                                }
 
    Serial.println();
    delay(5000);

    
    leer_boton();
    grabar_datos();
    
    
    enviar_post();
    //led_valor=!led_valor;
    //Serial.println(led_valor);
    
    delay(5000);

}